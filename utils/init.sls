install_packages:
  pkg.installed:
    - pkgs:
      - vim-enhanced
      - rsync
      - lftp
      - curl

modify_example_config:
  file.replace:
    - name: /root/example
    - pattern: 'example'
    - repl: 'salt'

copy_config:
  file.managed:
    - name: /etc/yum.repos.d/docker-ce.repo
    - source: salt://configs/aptly.repo
    - makedirs: True
